#EasyLog

## 说明

EasyLog使用很简单只需要将`EasyLog.h`包含进您的工程中就可以了。

## 使用事例

    EasyLog::GetInstance()->onLogChange = [=](EasyLog::LOG_LEVEL level, std::string logText) -> void
    {
        std::cout << level << " " << logText;
    };
    
    LOGI("i'm %s", "sollyu");
    LOGE("I'm " << "sollyu");
    LOG_DEBUG("i'm %s", "sollyu");
    EasyLog::GetInstance()->WriteLog(EasyLog::LOG_DEBUG, "i'm %s", "sollyu");

## LOG样式
![EasyLog](http://i2.tietuku.com/5cc77441bbe446da.png)

## 更新

### 2014年11月26日
	
	彻底更新使用C++11
	增加 EASY_LOG_DISABLE_LOG 控制是否输出LOG
	更新代码说明

### 2014年11月24日

	使用C++11
	增加 onLogChange 回调函数
	增加 EASY_LOG_FILE_NAME 宏定义
	增加 WINDOWS 下的支持

## 开源

代码地址：<https://git.oschina.net/sollyu/EasyLog.git>

博客地址：<http://www.sollyu.com/EasyLog>